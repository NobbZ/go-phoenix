package transport

import (
	"github.com/gorilla/websocket"
)

type ws struct {
	conn *websocket.Conn
}

var _ Transport = (*ws)(nil)

func websocketConnect(url string) (*ws, error) {
	w := new(ws)

	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		return nil, err
	}
	w.conn = conn

	return new(ws), nil
}

func (w *ws) Close() error {
	return nil
}

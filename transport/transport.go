package transport

import (
	"fmt"
	"io"
	"strings"
)

type Transport interface {
	io.Closer
}

func Connect(url string) (Transport, error) {
	parts := strings.Split(url, ":")

	switch prot := parts[0]; prot {
	case "ws":
		return websocketConnect(url)
	default:
		return nil, fmt.Errorf(`Protocol "$v" not supported`, prot)
	}
}
